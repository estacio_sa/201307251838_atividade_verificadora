from cefpython3 import cefpython as cef

class CEF:
    def __init__(self):
        cef.Initialize()
        browser = cef.CreateBrowserSync(url="https://www.google.com")
        cef.MessageLoop()

if __name__ == '__main__':
    app = CEF()
