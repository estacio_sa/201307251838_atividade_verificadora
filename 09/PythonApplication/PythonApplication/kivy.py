from kivy.app import App
from kivy.uix.label import Label

class Kivy(App):
    def build(self):
        return Label(text="Ola, Kivy!")

if __name__ == '__main__':
    app = Kivy()
    app.run()
