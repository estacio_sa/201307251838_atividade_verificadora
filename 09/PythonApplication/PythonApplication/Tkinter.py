import tkinter as tk

class Tkinter:
    def __init__(self, root):
        self.root = root
        self.root.title("Exemplo de Tkinter")

        label = tk.Label(root, text="Ola, Tkinter!")
        label.pack()

if __name__ == "__main__":
    root = tk.Tk()
    app = Tkinter(root)
    root.mainloop()
