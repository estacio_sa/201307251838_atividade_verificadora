from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlText

class MyForm(BaseWidget):

    def __init__(self):
        super().__init__('Hello World Example')

        # Adicione controles � interface
        self._text = ControlText('Texto', 'Ola Mundo')

if __name__ == "__main__":
    from pyforms import start_app
    start_app(MyForm)