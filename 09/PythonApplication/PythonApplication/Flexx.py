from flexx import flx

class Flexx(flx.Widget):
    def init(self):
        with flx.VBox():
            flx.Label(text="Ola, Flexx!")

if __name__ == '__main__':
    app = flx.App(Flexx)
    app.launch()
    flx.run()