import tkinter as tk
import psycopg2
from tkinter import messagebox

# Conex�o com o banco de dados
conn = psycopg2.connect(
    dbname='base01',
    user='postgres',
    password='#sdn7',
    host='192.168.88.224'  # ou o host do seu banco de dados
)

# Fun��o para criar um registro de nota
def criar_nota():
    nome = nome_entry.get()
    disciplina = disciplina_entry.get()
    nota = float(nota_entry.get())
    
    if not nota:
        nota = 0
    else:
        nota = float(nota_entry.get())


    if nome and disciplina and nota:
        cursor = conn.cursor()
        cursor.execute("INSERT INTO notas (nome_aluno, disciplina, nota) VALUES (%s, %s, %s)", (nome, disciplina, nota))
        conn.commit()
        cursor.close()
    else: 
        messagebox.showerror("Erro", "Preencha todos os campos (Nome, Disciplina e Nota)")

    listar_notas()

# Fun��o para listar todas as notas
def listar_notas():
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM notas")
    notas = cursor.fetchall()
    cursor.close()
    lista_notas.delete(0, tk.END)

    for nota in notas:
        lista_notas.insert(tk.END, nota)

# Fun��o para atualizar uma nota
def atualizar_nota():
    nova_nota = float(nova_nota_entry.get())
    if nova_nota:
         selecionado = lista_notas.curselection()    
         if selecionado:
            id_selecionado = lista_notas.get(lista_notas.curselection())[0]
            nova_nota = float(nova_nota_entry.get())
            cursor = conn.cursor()
            cursor.execute("UPDATE notas SET nota = %s WHERE id = %s", (nova_nota, id_selecionado))
            conn.commit()
            cursor.close()
         else:   
            messagebox.showerror("Erro", "Selecione o item na lista dando duplo clique !!!")     
    else:
        messagebox.showerror("Erro", "Digite a nova nota no campo abaixo")    
    listar_notas()

# Fun��o para excluir uma nota
def excluir_nota():
    selecionado = lista_notas.curselection()    
    if selecionado:
        id_selecionado = lista_notas.get(lista_notas.curselection())[0]
        cursor = conn.cursor()
        cursor.execute("DELETE FROM notas WHERE id = %s", (id_selecionado,))
        conn.commit()
        cursor.close()
    else:
        messagebox.showerror("Erro", "Selecione uma nota para excluir")    
    listar_notas()

def preencher_campos():
    selecionado = lista_notas.curselection()
    if selecionado:
        item_selecionado = lista_notas.get(selecionado[0])
        nome_entry.delete(0, tk.END)
        disciplina_entry.delete(0, tk.END)
        nota_entry.delete(0, tk.END)
        nome_entry.insert(0, item_selecionado[1])
        disciplina_entry.insert(0, item_selecionado[2])
        nota_entry.insert(0, str(item_selecionado[3]))


# Configura��o da janela
janela = tk.Tk()
janela.title("Sistema de Notas")

# Entradas
tk.Label(janela, text="Nome do Aluno").grid(row=0, column=0)
nome_entry = tk.Entry(janela)
nome_entry.grid(row=0, column=1)

tk.Label(janela, text="Disciplina").grid(row=1, column=0)
disciplina_entry = tk.Entry(janela)
disciplina_entry.grid(row=1, column=1)

tk.Label(janela, text="Nota").grid(row=2, column=0)
nota_entry = tk.Entry(janela)
nota_entry.insert(0, "0")
nota_entry.grid(row=2, column=1)

# Bot�es
criar_botao = tk.Button(janela, text="Criar Nota", command=criar_nota)
criar_botao.grid(row=3, column=0)

atualizar_botao = tk.Button(janela, text="Atualizar Nota", command=atualizar_nota)
atualizar_botao.grid(row=3, column=1)

excluir_botao = tk.Button(janela, text="Excluir Nota", command=excluir_nota)
excluir_botao.grid(row=3, column=2)

# Lista de notas
lista_notas = tk.Listbox(janela, width=50)
lista_notas.grid(row=4, columnspan=2)
lista_notas.bind("<<ListboxSelect>>", preencher_campos())  # Vincular evento de sele��o

# Lista de notas
lista_notas = tk.Listbox(janela, width=50)
lista_notas.grid(row=4, columnspan=3)

# Entrada para a nova nota (atualiza��o)
tk.Label(janela, text="Nova Nota").grid(row=5, column=0)
nova_nota_entry = tk.Entry(janela)
nova_nota_entry.insert(0, "0")
nova_nota_entry.grid(row=5, column=1)

# Bot�o para listar notas
listar_botao = tk.Button(janela, text="Listar Notas", command=listar_notas())
listar_botao.grid(row=6, columnspan=3)

janela.mainloop()
