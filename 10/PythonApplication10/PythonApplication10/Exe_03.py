import tkinter as tk

def obter_texto():
    texto = entrada.get()
    label.config(text="Voce digitou: " + texto)

janela = tk.Tk()
janela.title("Exemplo de Entrada de Texto")

label = tk.Label(janela, text="Digite algo:")
label.pack()

entrada = tk.Entry(janela)
entrada.pack()

botao = tk.Button(janela, text="Obter Texto", command=obter_texto)
botao.pack()

janela.mainloop()
