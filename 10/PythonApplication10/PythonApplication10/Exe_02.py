import tkinter as tk

def mostrar_mensagem():
    label.config(text="Ola, Tkinter!")

janela = tk.Tk()
janela.title("Exemplo com Rotulo e Botao")

label = tk.Label(janela, text="Clique no botao abaixo")
label.pack()

botao = tk.Button(janela, text="Clique Aqui", command=mostrar_mensagem)
botao.pack()

janela.mainloop()
